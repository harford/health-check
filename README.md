# Endpoint Monitor

I implemented this as a flask app that has an endpoint that returns JSON with
the details.

The app creates a HealthTracker object which is used to poll the HTTP endpoint.
This creates a thread that polls the endpoint regularly and saves the result.

## Platform

Linux, Python 3.6

## Installation

```
pip install --user pipenv
pipenv install --python 3.6
```

## Usage:

```
$ pipenv shell
$ python health-check/run.py
```

The app can be tuned with environment variables:

* `LOG_LEVEL`: DEBUG, INFO, WARN, ERROR
* `URL_CHECK`: The url to check
* `POLL_TIME`: The time to wait between polling
* `REQUEST_TIMEOUT`: The time to wait for a valid response to be returned 

## Status:

The endpoint returns JSON with a `success_rate` and details about the failures.

```
$ curl localhost:5000 2> /dev/null | jq .
{
  "details": {
    "200": 3,
    "500": 1
  },
  "success_rate": 75
}
```

## Testing:

`doctest` is used to implement tests, and can be run by:

```
$ pipenv install --dev
$ pytest --doctest-modules \
	       --cov=. \
	       --cov-report term-missing \
	       --flake8 \
	       --pep8 \
	       health-check/check.py health-check/app/routes.py
```

