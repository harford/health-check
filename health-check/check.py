"""
Worker to schedule and track health requests
"""

from threading import Event, Thread, Lock
from collections import deque, Counter
import requests


RESULT_LENGTH = 100


class HealthTracker:
    """
    Main class for tracking website health

    Creates a HealthRequester object that is a thread to poll the url.
    """

    def __init__(self, url, logger, poll_time, request_timeout):
        self.logger = logger
        if self.logger is not None:
            self.logger.info("Starting HealthTracker")

        self.status = deque(maxlen=RESULT_LENGTH)

        self.lock = Lock()
        self.exit = Event()

        self.requester = HealthRequester(
            self.lock, self.exit, url, self.status, logger, poll_time, request_timeout
        )

    def get_results(self):
        """
        Returns a counter of current statistics

        >>> t = HealthTracker('', None, 0, 0)
        >>> t.status.append(200)
        >>> t.status.append(200)
        >>> t.status.append(500)
        >>> t.status.append(0)
        >>> t.get_results()
        Counter({200: 2, 500: 1, 0: 1})
        """

        with self.lock:
            return Counter(self.status)

    def start(self):
        """
        Starts the requester threads
        """

        self.requester.start()

    def stop(self):
        """
        Stops any requesters
        """

        self.exit.set()


class HealthRequester(Thread):
    """
    Thread for making actual requests
    """

    def __init__(
        self, lock, exit_flag, url, status, logger, poll_time, request_timeout
    ):
        self.wait_time = poll_time
        self.timeout = request_timeout

        self.lock = lock
        self.exit = exit_flag
        self.status = status
        self.url = url
        self.logger = logger

        if self.logger is not None:
            self.logger.info(f"HealthRequester for {url}, polling every {self.wait_time} s")

        super().__init__()

    def run(self):
        while not self.exit.is_set():
            result = self.get_status()

            self.logger.debug(f"Response: {result}")

            with self.lock:
                self.status.append(result)

            self.exit.wait(self.wait_time)

    def get_status(self):
        """
        Retrieves the status of the URL

        FIXME: Mock requests module so it can return 200 / 500 / timeout
        """

        try:
            req = requests.get(self.url, timeout=self.timeout)
            return req.status_code
        except requests.exceptions.RequestException as e:
            self.logger.info(f"Failed to get response: {e}")
            return 0
