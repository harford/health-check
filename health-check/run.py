"""
Main entry to flask app
"""

from app import app, tracker


if __name__ == "__main__":
    tracker.start()
    app.run(threaded=True)
    tracker.stop()
