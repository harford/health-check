import logging

from flask import Flask
from config import Config

from check import HealthTracker

app = Flask(__name__)
app.config.from_object(Config)


level = logging.getLevelName(app.config["LOG_LEVEL"])
app.logger.setLevel(level)

tracker = HealthTracker(
    url=app.config["URL_CHECK"],
    logger=app.logger,
    poll_time=app.config["POLL_TIME"],
    request_timeout=app.config["REQUEST_TIMEOUT"],
)


from app import routes
