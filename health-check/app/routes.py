"""Health Check App Routes"""

from flask import jsonify

from app import app, tracker


@app.route("/healthz", methods=["GET"])
def healthz():
    """
    Health endpoint
    """

    status = {"status": "OK"}

    return jsonify(status)


def get_success_rate(results):
    """
    Calculates the success rate percentage from the results

    >>> q = {200: 1}
    >>> get_success_rate(q)
    100.0
    >>> q[0] = 9
    >>> get_success_rate(q)
    10.0
    """

    success = results[200]

    total = 0.0

    for result in results.values():
        total += result

    return success * 100.0 / total


@app.route("/", methods=["GET"])
def index():
    """
    Index endpoint
    """

    results = tracker.get_results()

    app.logger.debug(results)

    status = {"success_rate": get_success_rate(results), "details": results}

    return jsonify(status)
