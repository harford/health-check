"""
Health Check's environment variables
"""

import os


class Config:
    """
    Flask configuration variables
    """

    LOG_LEVEL = os.environ.get("LOG_LEVEL", "INFO")
    URL_CHECK = os.environ.get("URL_CHECK", "http://localhost:12345")
    POLL_TIME = os.environ.get("POLL_TIME", 6.0)
    REQUEST_TIMEOUT = os.environ.get("REQUEST_TIMEOUT", 0.1)
