.PHONY: run test

APP_NAME=health-check

run: test
	python health-check/run.py

TESTED_FILES = \
	       app/routes.py \
	       check.py

test:
	pytest --doctest-modules \
	       --cov=. \
	       --cov-report term-missing \
	       --flake8 \
	       --pep8 \
	       $(addprefix $(APP_NAME)/, $(TESTED_FILES))

reformat:
	black $(APP_NAME)
